package middleware

import (
	"carto/utils"
	"carto/utils/config"
	"carto/utils/tokens"

	"github.com/gofiber/fiber/v2"
	"github.com/rs/zerolog/log"
)

func Check(c *fiber.Ctx) error {
	// Check if oauth is configured
	if config.Conf.Oauth.Server != "" {
		return oauth2(c)
	}

	token := c.Cookies("token")
	t, err := tokens.Verify(token)
	if err != nil {
		log.Err(err).Msg(err.Error())
		return c.SendStatus(fiber.ErrForbidden.Code)
	} else {
		if string(t.Token) != "" {
			return c.Next()
		} else {
			utils.DelCookie(c, "token")
			return c.Redirect("/")
		}
	}
}
