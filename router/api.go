package router

import (
	"carto/controllers/auth"
	"carto/controllers/layers"
	"carto/controllers/maps"
	"carto/middleware"

	"github.com/gofiber/fiber/v2"
)

func Api(app *fiber.App) {
	api := app.Group("/api")

	/* AUTH CONTROLLER -> /api/auth */
	authRoute := api.Group("/auth")
	authRoute.Post("/register", auth.Register)
	authRoute.Post("/login", auth.Login)
	authRoute.All("/logout", auth.Logout)

	oauthRoute := authRoute.Group("/oauth2")
	oauthRoute.Get("/", auth.Oauth2)
	oauthRoute.Get("/callback", auth.Oauth2Callback)

	/* MAP CONTROLLER -> /api/map */
	mapRoute := api.Group("/map", middleware.Check)

	mapRoute.Get("/", maps.List)         // List the maps of an editor
	mapRoute.Post("/", maps.Create)      // Create a new map
	mapRoute.Delete("/:id", maps.Delete) // Delete a map

	mapRoute.Put("/editors/:id", maps.Editor)    // Add an editor to the map
	mapRoute.Delete("/editors/:id", maps.Editor) // Remove an editor from the map

	mapRoute.Put("/layers/:id", maps.Layers)    // Add a layer to the map
	mapRoute.Delete("/layers/:id", maps.Layers) // Remove a layer from the map

	/* LAYER CONTROLLER -> /api/layer */
	layerRoute := api.Group("/layer", middleware.Check)

	layerRoute.Get("/", layers.List)         // List the layers of an editor
	layerRoute.Post("/", layers.Create)      // Create a new layer
	layerRoute.Delete("/:id", layers.Delete) // Delete a layer

	layerRoute.Put("/editors/:id", layers.Editor)    // Add an editor to a layer
	layerRoute.Delete("/editors/:id", layers.Editor) // Remove an editor from a layer
}
