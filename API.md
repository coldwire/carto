# Carto API

### Result
The API alway return 4 fields: 
```json
{
	"status": "<STATUS_OF_THE_RESPONSE>",
	"message": "<just a message to describe the result>",
	"content": "<can be a string, an array, object or just null, depend of the kind of reponse>",
	"error": "<most of the time empty but can containe more informations about an error>"
}
```

### Auth **/api/auth**
> ### **POST** /register
> To register a new user to the instance
> ```json
> {
>   "username": "<username>",
>   "password": "<password">
> }
> ```
>
> Result on success:
> ```json
> {
>   "status": "SUCCESS",
> 	"message": "<bla bla>",
> 	"content": {
>     "user": {
>       "username": "<username>",
>       "role": "<role>"
>     },
>     "token": "<jwt token>"
>   },
>   "error": null
> }
> ```

> ### **POST** /login
> To login the instance
> ```json
> {
>   "username": "<username>",
>   "password": "<password">
> }
> ```
>
> Result on success:
> ```json
> {
>   "status": "SUCCESS",
> 	"message": "<bla bla>",
> 	"content": {
>     "user": {
>       "username": "<username>",
>       "role": "<role>"
>     },
>     "token": "<jwt token>"
>   },
>   "error": null
> }
> ```

> ### **POST** /oauth/callback
> Url to set as callback url for openid providers

> ### **GET** /logout
> To logout of the instance

> ### **GET** /oauth
> To get an authorization url
>
> Result on success:
> ```json
> {
>   "status": "SUCCESS",
>   "message": "<bla bla>",
>   "content": {
>     "enabled": true,
>     "redirect_url": "<authorization url>"
>   },
>   "error": null
> }
> ```

### Maps **/api/maps**
> ### **POST** /
> Create a new map
> ```json
> {
>   "title": "<title of the map>",
>   "description": "<description of the map>",
>   "password": "<password of the map, optional>",
>   "lat": 51.505, // Initial latitude for the view
>   "long": -0.09, // Initial longitude for the view
>   "zoom": 13 // Initial zoom for the view
> }
> ```

> ### **DELETE** /:map
> Delete a map with it ID

### Layers **/api/layers**
> ### **POST** /
> Create a new layer
> ```json
> {
>   "title": "<title>",
>   "description": "<description>",
>   "icon": "<base64 icon>",
>   "type": "<organization | event>",
> }
> ```

> ### **DELETE** /:id
> Delete a specific layer and all its markers

> ### **GET** /list/:map
> List layers of a map
>
> Result on success:
>```json
>  {
>    "status": "SUCCESS",
>    "message": "<bla bla>",
>    "content": {
>      "layers": [
>        {
>          "id": "<id of the layer>",
>          "title": "<title>",
>          "description": "<description>",
>          "icon": "<base64 icon>",
>          "type": "<organization | event>"
>        }
>      ]
>    },
>    "error": null
>  }
>```

### Markers **/api/markers**
> ### **POST** /
> Create a new marker
> ```json
> {
>   "layer": "<layer id>",
>   "title": "<title of the marker>",
>   "description": "<description of the marker>",
>   "links": [
>     "https://socialmedia.tld",
>     "https://socialmedia2.tld"
>   ],
>   "long": 13,
>   "lat": 12
> }
> ```

> ### **DELETE** /:id
> Delete a specific marker

> ### **GET** /list/:map
> Get every makers from every layers of a map
>
> Result on success:
>```json
>  {
>    "status": "SUCCESS",
>    "message": "<bla bla>",
>    "content": {
>      "map": "<id of the map the layers belong to>",
>      "layers": [
>        {
>          "id": "<id of the layer>",
>          "title": "<title>",
>          "description": "<description>",
>          "icon": "<base64 icon>",
>          "type": "<organization | event>",
>          "markers": [
>            {
>              "title": "<title of the marker>",
>              "description": "<description of the marker>",
>              "links": [
>                "https://socialmedia.tld",
>                "https://socialmedia2.tld"
>              ],
>              "long": 13,
>              "lat": 12
>            }
>          ]
>        }
>      ]
>    },
>    "error": null
>  }
>```