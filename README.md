# carto

Make maps for events, demonstrations, local groups of an organization

## Get started
```sh
go run main.go -config config.toml
```