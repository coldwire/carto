module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      backgroundImage: {
        'login': "linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), url('./../assets/background.jpg')",
      },
      colors: {
        green: {
          100: '#009639',
          25: 'rgba(0, 143, 100, 0.25)',
          10: 'rgba(0, 143, 100, 0.10)'
        },
        blue: {
          100: '#0061ff',
          50: 'rgba(0, 97, 255, 0.50)',
          75: 'rgba(0, 97, 255, 0.75)',
          25: 'rgba(0, 97, 255, 0.25)',
          10: 'rgba(0, 97, 255, 0.10)'
        },
        gray: {
          1: '#0d1117',
          2: '#161b22',
          3: '#30363d',
          4: 'rgba(240, 246, 252, 0.50)',
          5: 'rgba(240, 246, 252, 0.25)',
          6: 'rgba(240, 246, 252, 0.05)',
          7: '#F0F6FC'
        }
      },
      animation: {
        fadein: 'fadein 0.5s',
        fadeout: 'fadeout 0.5s',
        fadeinout: 'fadein 0.5s, fadeout 0.5s 2.5s',
        width: 'width 2s'
      },
      transition: {
        width: 'width'
      }
    },
    fontFamily: {
      sans: ['Montserrat', 'sans-serif']
    }
  },
  plugins: [],
}
