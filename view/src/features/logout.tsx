import { FunctionalComponent, h } from 'preact';
import { Link, route } from 'preact-router';
import { useEffect } from 'preact/hooks';
import useStore from './store/store';

const Logout: FunctionalComponent = () => {

    const [username, logout] = useStore((store) => [store.user?.username, store.logout]);

    useEffect(() => {
        logout();
    }, []);

    useEffect(() => {
        if (!username) {
            route('/');
        }
    }, [username]);

    return (
        <div class="h-screen">
            welcome {username}
            <Link href='/sign-in'>LOGIN</Link>
        </div>
    );
};

export default Logout;
