import { FunctionalComponent, h } from 'preact';
import Button from '../components/button';
import { useText } from 'preact-i18n';
import { LifeBuoy, Loader2, Lock, LogIn, User } from 'lucide-react';
import { Helmet } from "react-helmet";
import { useEffect, useState } from 'preact/hooks';
import TextField from '../components/textfield';
import Router, { Link, Route, route } from 'preact-router';
import Trans from '../components/custom-text';
import Password from '../components/password';
import useStore from './store/store';
import Layout from '../components/layout';

const MapList: FunctionalComponent = () => {

    const [isLoading, user, register] = useStore((store) => [store.isLoading, store.user, store.register]);

    useEffect(() => {
        if (user && user.username) {
            route('/app');
        }
    }, [user])

    return (
        <Layout/>
    );
};

export default MapList;
