import { FunctionalComponent, h } from 'preact';
import { Link } from 'preact-router';
import useStore from './store/store';

const Homepage: FunctionalComponent = () => {

    const [username] = useStore((store) => [store.user?.username]);

    return (
        <div class="h-full">
            welcome {username}
            <Link href='/sign-in'>LOGIN</Link>
        </div>
    );
};

export default Homepage;
