import { FunctionalComponent, h } from 'preact';
import Button from '../components/button';
import { useText } from 'preact-i18n';
import { LifeBuoy, Loader2, Lock, LogIn, Unlock, User } from 'lucide-react';
import { Helmet } from "react-helmet";
import { useEffect, useState } from 'preact/hooks';
import TextField from '../components/textfield';
import { Link, route } from 'preact-router';
import Trans from '../components/custom-text';
import Password from '../components/password';
import useStore from './store/store';
import Layout from '../components/layout';


const Login: FunctionalComponent = () => {

    const { title, tUsername, loginWith } = useText({ 
        title: "login.title", 
        tUsername: "login.username", 
        loginWith: 'login.loginWith' 
    });

    const [username, setUsername] = useState<string>('');
    const [password, setPassword] = useState<string>('');
    
    const [isLoading, user, login] = useStore((store) => [store.isLoading, store.user, store.login]);

    useEffect(() => {
        if (user && user.username) {
            route('/app');
        }
    }, [user]);

    return (
        <div class="bg-login">
            <Helmet title={`Carto - ${title}`} meta={[
                { name: 'test', content: 'lol'}
            ]} />

            <Layout bgImage />

            <div class="fixed z-10 left-0 right-0 sm:left-40 top-0 sm:right-auto">
                <div class="h-screen justify-center content-center grid animate-fadein">
                    <div class="bg-white dark:bg-gray-2 shadow rounded-lg m-1 overflow-hidden">
                        <div class={"bg-green-25 text-green-100 flex justify-center items-center px-6 py-4"}>
                            <Lock size={16} />
                            <p class="ml-2">
                                <Trans id="login.message" />
                            </p>
                        </div>

                        <div class="p-6 border-b dark:border-gray-5">

                            <h1 class="text-2xl font-black mb-10">
                                <Trans id="login.title" />
                            </h1>

                            <form>
                                <TextField value={username} onChange={setUsername} placeholder={tUsername} icon={<User size={20} />} pattern='[a-zA-Z]{3,}' />
                                <Password value={password} onChange={setPassword} />
                                <Button 
                                    title={isLoading ? '' : <Trans id="login.title" />} 
                                    onClick={() => login(username, password)} 
                                    color='primary' 
                                    disabled={!username.length || !password.length}
                                    startIcon={isLoading ? <div class="animate-spin"><Loader2 /></div> : undefined} />
                            </form>

                            <p class="mt-3 dark:text-gray-4 text-xs">
                                <Trans id="login.noAccount" />&nbsp;
                                <Link class='underline' href='/sign-up'>
                                    <Trans id="login.createAccount" />
                                </Link>
                            </p>
                        </div>

                        <div class="p-6 border-b dark:border-gray-5">
                            <Button title={loginWith + " X"} link="/404" startIcon={<LogIn />} color='secondary' />
                        </div>

                        <div class="p-6 flex justify-between">
                            <Button title={<Trans id="login.forgotPassword" />} link="/404" startIcon={<Unlock size={16} />} color='none' />
                            <Button title={<Trans id="login.help" />} link="/404" startIcon={<LifeBuoy size={16} />} color='none' />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Login;
