// const superagent = require('superagent');
import superagent from 'superagent';
import useStore from './store';

const createSnackbar = (message: string) => {
    if (message && message != 'SUCCESS') {
        useStore.setState({ snackbar: { message: "errors." + message, type: 1 }, isLoading: false });
    } else {
        useStore.setState({ snackbar: { message: "errors.ERROR_UNKNOWN", type: 1 }, isLoading: false });
    }
};

export const register = (username: string, password: string): void => {
    superagent
        .post('/api/auth/register')
        .send({ username, password })
        .then((e) => useStore.setState({ user: e.body.content.user, isLoading: false }))
        .catch((e) => createSnackbar(e.response.body.status as string));
};

export const login = (username: string, password: string): void => {
    superagent
        .post('/api/auth/login')
        .send({ username, password })
        .then((e) => useStore.setState({ user: e.body.content, isLoading: false }))
        .catch((e) => createSnackbar(e.response.body.status as string));
};

export const logout = (): void => {
    superagent
        .get('/api/auth/logout')
        .then((_) => useStore.setState({ user: undefined }, true))
        .catch((e) => createSnackbar(e.response.body.status as string));
};

export const getOAuth = (): void => {
    superagent
        .get('/api/auth/oauth')
        .then((e) => useStore.setState({ oauth: e.body.content }))
        .catch((e) => createSnackbar(e.response.body.status as string));
};