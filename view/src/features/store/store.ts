import create, { State } from 'zustand'
import { getOAuth, login, logout, register } from './service';
// import { combine } from "zustand/middleware";
import { persist, devtools } from "zustand/middleware";

export interface ReduxState extends State {
    isLoading?: boolean, 

    locale?: string,
    setLocale: (newLocale: string) => void,

    snackbar?: {
        type: number,
        message: string,
    },
    toggleSnackbar: (type: number, message: string) => void,

    oauth?: {
        enabled?: boolean,
        redirect_url?: string
    },
    getOAuth: () => void,

    user?: {
        username?: string,
        role?: string,
        token?: string,
    },
    login: (username: string, password: string) => void,
    register: (username: string, password: string) => void,
    logout: () => void,
}

const useStore = create<ReduxState>()(devtools(persist(
    (set) => ({
        setLocale: (newLocale: string) => set({ locale: newLocale }),
        toggleSnackbar: (type: number, message: string) => set({ snackbar: { type, message } }),
        register: async (username: string, password: string) => {
            set({ isLoading: true });
            await register(username, password);
        },
        login: async (username: string, password: string) => {
            set({ isLoading: true });
            await login(username, password);
        },
        getOAuth: async () => await getOAuth(),
        logout: async () => await logout(),
    }),
    { name: 'carto-storage' }
)));

export default useStore;