import { FunctionalComponent, h, JSX } from 'preact';
import { IntlProvider } from 'preact-i18n';
import { useEffect, useState } from 'preact/hooks';
import useStore from '../store/store';
import shallow from 'zustand/shallow';
import { Definition } from './types';

interface I18nProps {
    children: JSX.Element[];
}

/**
 * The component is based upon preact-i18n
 * The current locale is stored in the store for more
 * flexibility, as it will allow the user to choose
 * it (TODO later).
 * 
 * See also :
 * - https://www.npmjs.com/package/preact-i18n
 * - https://limhenry.medium.com/how-to-add-i18n-to-preact-dff1bf19917
 */
const I18n: FunctionalComponent<I18nProps> = (props) => {

    const [locale, setLocale, toggleSnackbar] = useStore((state) => [state.locale, state.setLocale, state.toggleSnackbar], shallow);

    const [definition, setDefinition] = useState<Definition | undefined>(undefined);

    useEffect(() => {
        if (locale) {
            import(`./locales/${locale}.json`)
                .then(definition => setDefinition(definition.default))
                .catch(() => {
                    toggleSnackbar(1, 'The app is not available in your language');
                    setLocale('en');
                });
        }
    }, [locale])

    useEffect(() => {
        let userLang = navigator.language.split('-')[0];
        setLocale(userLang);
    }, []);

    return (
        <IntlProvider definition={definition}>
            {props.children}
        </IntlProvider>
    );
};

export default I18n;
