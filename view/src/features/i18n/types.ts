export type Definition = {
    "global": {
        "appName": string,
    },
    "login": {
        "title": string,
        "subtitle": string,
        "message": string,
        "username": string,
        "password": string,
        "noAccount": string,
        "createAccount": string,
        "loginWith": string,
        "help": string,
        "forgotPassword": string,
    },
    "notfound": {
        "title": string,
        "subtitle": string,
        "button": string,
    },
    "register": {
        "alreadyHave": string,
        "register": string,
    },
    "errors": {
        "noTranslation": string,
        "ERROR_REQUEST": string,
        "ERROR_BODY": string,
        "ERROR_UNKNOWN": string,
        "ERROR_PERMISSION": string,
        "ERROR_DB": string,
        "ERROR_AUTH": string,
        "ERROR_AUTH_EXIST": string,
        "ERROR_AUTH_PASSWORD": string
    }
}