import { FunctionalComponent, h } from 'preact';
import { useEffect } from 'preact/hooks';
import Router, { Route, route } from 'preact-router';
import useStore from './store/store';
import MapList from './map-list';

const RestrictedArea: FunctionalComponent = () => {

    const [user] = useStore((store) => [store.user]);

    useEffect(() => {
        if (!user || !user.username) {
            route('/');
        }
    }, [user])

    return (
        <Router>
            <Route path="/app" component={MapList} />
        </Router>
    );
};

export default RestrictedArea;
