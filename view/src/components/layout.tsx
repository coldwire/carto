import { ChevronsRight } from 'lucide-react';
import { FunctionalComponent, h } from 'preact';
import { useState } from 'preact/hooks';
import FixedLogo from './fixed-logo';

interface LayoutProps {
    leftComponents?: JSX.Element;
    rightComponents?: JSX.Element;
    animate?: boolean;
    bgImage?: boolean;
}

const Layout: FunctionalComponent<LayoutProps> = (props) => {

    const [open, setOpen] = useState<boolean>(false);

    return (
        <div class='flex h-screen'>
            <div class={`${!props.bgImage ? 'bg-slate-400 dark:bg-gray-3' : 'sm:bg-slate-400 sm:dark:bg-gray-3'} pt-14 pl-6 w-20 sm:w-80 open:w-3/4 transition duration-500 transition-[width] fixed h-screen overflow-hidden`} open={open}>
                <FixedLogo />
                <div class="overflow-hidden">
                    {props.leftComponents}
                </div>
                
                {
                    props.animate &&
                    <div onClick={() => setOpen(!open)} class={`transition duration-500 fixed max-w-fit left-6 bottom-6 sm:invisible ${open && 'rotate-180'} text-gray-7 cursor-pointer`}>
                        <ChevronsRight size={36} />
                    </div>
                }
            </div>
            <div class={`ml-24 sm:ml-80 ${props.bgImage && 'bg-login'}`}>
                {props.rightComponents}
            </div>
        </div>
    );
};

export default Layout;
