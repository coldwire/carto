import { Fragment, FunctionalComponent, h } from 'preact';
import { Helmet } from 'react-helmet';
import Button from './button';
import Trans from './custom-text';
import Layout from './layout';

const Notfound: FunctionalComponent = () => {
    return (
        <Fragment>
            <Helmet title={`Carto - 404`} />

            <Layout
                rightComponents={
                    <div class='animate-fadein w-full h-full flex items-center pl-5 sm:pl-10'>
                        <div>
                            <h1 class='text-6xl font-mono'><Trans id="notfound.title" /></h1>
                            <p class='text-3xl font-mono'><Trans id="notfound.subtitle" /></p>
                            <div className="mt-5 w-1/3 min-w-fit">
                                <Button link='/' title={<Trans id="notfound.button" />} color='secondary' />
                            </div>
                        </div>
                    </div>
                } 
                 />
        </Fragment>
    );
};

export default Notfound;
