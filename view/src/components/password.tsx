import { Eye, EyeOff, Key } from 'lucide-react';
import { Fragment, FunctionalComponent, h } from 'preact';
import { useText } from 'preact-i18n';
import { useState } from 'preact/hooks';

interface TextFieldProps {
    value: string;
    onChange: (value: string) => void;
    editMode?: boolean;
}

const Password: FunctionalComponent<TextFieldProps> = (props) => {

    const [showPassword, setShowPassword] = useState<boolean>(false);
    const [strength, setStrength] = useState<number>(0);

    const { tPassword } = useText({ tPassword: 'login.password' });

    const changePassword = (pwd: string) => {
        var check, ltr, i, l;
        var variation = 0;
        var letters: {[key: string]: number} = {};
        var score = 0;

        if (pwd && pwd.length) {
            /* Score character variation */
            var variations: {[key: string]: boolean} = {
                'lower': /[a-z]/.test(pwd),
                'upper': /[A-Z]/.test(pwd),
                'nonWords': /\W/.test(pwd),
                'digits': /\d/.test(pwd)
            };

            for (check in variations) {
                variation += variations[check] ? 1 : 0;
            }

            score += (variation - 1) * 10;

            /* Score unique letters until 5 repetitions */
            for (i = 0, l = pwd.length; i < l; i++) {
                ltr = letters[pwd[i]] = (letters[pwd[i]] || 0) + 1;
                score += 5 / ltr;
            }

            /* Score length (about 8 chars for a safe password) */
            score -= 16 - (pwd.length / 16);
        }
        
        setStrength(score);
        props.onChange(pwd)
    }

    return (
        <Fragment>
            <label class="block my-3 relative">
                <span class="absolute inset-y-0 left-0 flex items-center pl-2 text-gr text-slate-400 dark:text-gray-4">
                    <Key size={20} />
                </span>
                <input 
                    type={showPassword ? 'text' : 'password'} 
                    placeholder={tPassword} 
                    pattern=".{9,}"
                    class="px-9 mt-1 block w-full px-3 py-2 rounded-md shadow-sm border dark:shadow-none font-semibold text-sm transition duration-200
                        bg-white dark:bg-gray-1 
                        placeholder-slate-400 dark:placeholder-gray-4
                        border-slate-300  dark:border-gray-5
                        focus:outline-none focus:border-green-100 focus:ring-1 focus:ring-green-100
                        disabled:bg-slate-50 disabled:text-slate-500 disabled:border-slate-200 disabled:shadow-none
                        invalid:border-pink-500 invalid:text-pink-600 focus:invalid:border-pink-500 focus:invalid:ring-pink-500"
                    value={props.value}
                    onChange={(e) => changePassword((e.target as HTMLInputElement).value)}
                />
                <span class="absolute inset-y-0 right-0 flex items-center pr-2 text-gr text-slate-400 dark:text-gray-4 cursor-pointer" onClick={() => setShowPassword(!showPassword)}>
                    { showPassword ? <Eye size={20} /> : <EyeOff size={20} />}
                </span>
            </label>

            {
                props.editMode &&
                <div class='flex -mx-1 mb-3'>
                    {
                        [0,25,50,65].map((i) => (
                            <div class="w-1/4 px-1">
                                <div class={`h-2 rounded-xl transition-colors ${i < strength ? (strength <= 25 ? 'bg-red-400' : (strength <= 65 ? 'bg-yellow-400' : 'bg-green-500')):'bg-gray-200'}`}></div>
                            </div>
                        ))
                    }
                </div>
            }
        </Fragment>
    );
};

export default Password;
