import { X } from 'lucide-react';
import { Fragment, FunctionalComponent, h } from 'preact';
import { Text } from 'preact-i18n';
import { useEffect } from 'preact/hooks';
import useStore from '../features/store/store';

/**
 * Snackbar Component : Display temporary information
 * at the bottom of the screen.
 * 
 * See also :
 * - https://dev.to/gthinh/make-your-own-snackbars-using-react-redux-and-styled-components-40kn
 * - https://github.com/g-thinh/simple-snackbar
 */
const Snackbar: FunctionalComponent = () => {

    const [snackbar, setSnackbar] = useStore((state) => [state.snackbar, state.toggleSnackbar]);

    let timer: NodeJS.Timeout;
    let show = Boolean(snackbar?.message.length)
    let color = snackbar?.type == 1 ? 'bg-red-500' : 'bg-green-100'

    function handleTimeout() {
        timer = setTimeout(() => {
            setSnackbar(0, '');
        }, 2900);
    }
  
    function handleClose() {
        clearTimeout(timer);
        setSnackbar(0, '');
    }
  
    useEffect(() => {
        if (show) {
            handleTimeout();
        }
        return () => {
            clearTimeout(timer);
        };
    }, [snackbar]);

    return (
        <Fragment>
            {
                show && (
                    <div class={`animate-fadeinout flex justify-center items-center shadow-lg text-white rounded-md fixed bottom-4 left-1/2 -translate-x-1/2 py-2 px-4 ${color}`}>
                        <p><Text id={snackbar?.message ?? ''}/></p>
                        <X onClick={handleClose} className={'ml-4'} />
                    </div>
                )
            }
        </Fragment>
    );
};

export default Snackbar;
