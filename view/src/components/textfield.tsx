import { FunctionalComponent, h } from 'preact';

interface TextFieldProps {
    placeholder?: string;
    value: string;
    type?: string;
    pattern?: string;
    required?: boolean;
    disabled?: boolean;
    label?: string;
    icon?: JSX.Element;
    onChange: (value: string) => void
}

const TextField: FunctionalComponent<TextFieldProps> = (props) => {

    return (
        <label class="block my-3 relative">
            {
                props.label &&
                <span class="block text-sm font-medium">{props.placeholder}</span>
            }
            {
                props.icon &&
                <span class="absolute inset-y-0 left-0 flex items-center pl-2 text-gr text-slate-400 dark:text-gray-4">
                    {props.icon}
                </span>
            }
            <input 
                type={props.type} 
                placeholder={props.placeholder} 
                required={props.required}
                disabled={props.disabled}
                pattern={props.pattern}
                class="pl-9 mt-1 block w-full px-3 py-2 rounded-md shadow-sm border dark:shadow-none font-semibold text-sm transition duration-200
                    bg-white dark:bg-gray-1 
                    placeholder-slate-400 dark:placeholder-gray-4
                    border-slate-300  dark:border-gray-5
                    focus:outline-none focus:border-green-100 focus:ring-1 focus:ring-green-100
                    disabled:bg-slate-50 disabled:text-slate-500 disabled:border-slate-200 disabled:shadow-none
                    invalid:border-pink-500 invalid:text-pink-600 focus:invalid:border-pink-500 focus:invalid:ring-pink-500"
                value={props.value}
                onChange={(e) => props.onChange((e.target as HTMLInputElement).value)}
            />
        </label>
    );
};

export default TextField;
