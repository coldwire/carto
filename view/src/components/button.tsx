import { FunctionalComponent, h } from 'preact';
import { Link } from 'preact-router';

interface ButtonProps {
    title: string | JSX.Element;
    link?: string;
    onClick?: () => void;
    fullwidth?: boolean;
    endIcon?: JSX.Element;
    startIcon?: JSX.Element;
    disabled?: boolean;
    color?: 'primary' | 'secondary' | 'none';
}

const Button: FunctionalComponent<ButtonProps> = (props) => {
    let color = '';
    switch(props.color) {
        case 'secondary':
            color = 'bg-slate-100 hover:bg-slate-200 border border-slate-300 focus:ring-slate-300 w-full px-3 py-2 dark:bg-gray-2 dark:hover:bg-gray-1 dark:border-gray-5';
            break;
        case 'primary':
            color = "bg-blue-100 hover:bg-blue-75 text-white focus:ring-blue-100 w-full px-3 py-2";
            break;
        case 'none':
            color = 'hover:bg-slate-100 focus:ring-slate-100 p-2 text-gray-3 text-xs dark:text-gray-4 dark:hover:bg-gray-1'
    }

    const onClick = () => {
        if (!props.disabled && props.onClick) {
            props.onClick();
        }
    }
        
    return (
        <Link class={`transition duration-200 flex items-center justify-center text-center ${color} ${!props.disabled && 'cursor-pointer'} focus:outline-none rounded-md`} href={props.link} disabled={props.disabled} onClick={onClick}>
            {props.startIcon}
            <span class='inline-block mx-2'>{props.title}</span>
            {props.endIcon}
        </Link>
    );
};

export default Button;
