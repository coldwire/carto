import { FunctionalComponent, h } from 'preact';
import logo from '../assets/icons/logo.svg';

const FixedLogo: FunctionalComponent = () => {
    return (
        <img src={logo} class="absolute left-6 top-6" />
    );
};

export default FixedLogo;
