import { FunctionalComponent, h } from 'preact';
import { Text } from 'preact-i18n';

interface TransProps {
    id: string;
}

const Trans: FunctionalComponent<TransProps> = (props) => {
    return (
        // @ts-ignore
        <Text id={props.id}><div class="loading"></div></Text>
    );
};

export default Trans;
