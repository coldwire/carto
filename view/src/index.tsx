import './style/index.css';

import { FunctionalComponent, h } from 'preact';
import { Route, Router } from 'preact-router';
import NotFoundPage from './components/notfound';
import Login from './features/login';
import I18n from './features/i18n';
import Snackbar from './components/snackbar';
import Register from './features/register';
import Homepage from './features/homepage';
import Logout from './features/logout';
import RestrictedArea from './features/restricted-area';

/**
 * All components are in components folder
 * 
 * See also :
 * - https://github.com/preactjs/preact-cli/issues/522#issuecomment-370835370 
 */
const App: FunctionalComponent = () => {
    return (
        <div id="preact_root" class="dark:text-white text-gray-3 font-semibold text-sm dark:bg-gray-2 bg-gray-4">
            <I18n>
                <Router>
                    <Route path="/" component={Homepage} />
                    <Route path="/app" component={RestrictedArea} />
                    <Route path="/log-out" component={Logout} />
                    <Route path="/sign-in" component={Login} />
                    <Route path="/sign-up" component={Register} />
                    <NotFoundPage default />
                </Router>
                <Snackbar />
            </I18n>    
        </div>
    );
};

export default App;
