package utils

import (
	"carto/database"
	"carto/models"

	"github.com/rs/zerolog/log"
)

func MigrateDatabse() {
	err := database.DB.AutoMigrate(
		models.Map{},
		models.Layer{},
		models.Marker{},
		models.User{},
	)

	if err != nil {
		log.Fatal().Msg(err.Error())
	}
}
