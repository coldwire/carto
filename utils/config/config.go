package config

import (
	"os"

	"carto/utils/env"

	"github.com/BurntSushi/toml"
	"github.com/joho/godotenv"
	"github.com/rs/zerolog/log"
)

// Export config to the whole project
var Conf *Config

// Init config to ensure that env variable are loaded
func Init(file string) {
	// if config file is specified we use it
	if file != "" {
		log.Info().Msg("Loading config file: " + file)
		f, err := os.ReadFile(file)
		if err != nil {
			log.Fatal().Msg(err.Error())
		}

		var c Config
		_, err = toml.Decode(string(f), &c)
		if err != nil {
			log.Fatal().Msg(err.Error())
		}

		Conf = &c

		return
	}

	err := godotenv.Load()
	if err != nil {
		log.Err(err).Msg(err.Error())
	}

	Conf = &Config{
		Server: ServerConfig{
			Address: env.Get("CARTO_SERVER_ADDRESS", "0.0.0.0"),
			Port:    env.Get("CARTO_SERVER_PORT", "3000"),
		},

		Database: DatabaseConfig{
			Driver: env.Get("CARTO_DATABASE_DRIVER", "sqlite"),
			Postgres: DatabasePostgresConfig{
				Address:  env.Get("CARTO_DATABASE_ADDRESS", "127.0.0.1"),
				Port:     env.Get("CARTO_DATABASE_PORT", "5432"),
				User:     env.Get("CARTO_DATABASE_USER", "postgres"),
				Password: env.Get("CARTO_DATABASE_PASSWORD", "123456789"),
				Name:     env.Get("CARTO_DATABASE_NAME", "carto"),
			},
			Sqlite: DatabaseSqliteConfig{
				Path: env.Get("CARTO_DATABASE_PATH", "/tmp/db.sqlite"),
			},
		},

		Oauth: OauthConfig{
			Server:   env.Get("CARTO_OAUTH_SERVER", ""),
			Id:       env.Get("CARTO_OAUTH_CLIENT", "carto"),
			Secret:   env.Get("CARTO_OAUTH_SECRET", ""),
			Callback: env.Get("CARTO_OAUTH_CALLBACK", "https://carto.coldwire.org/api/auth/oauth2/callback"),
		},
	}
}

type Config struct {
	Server   ServerConfig
	Database DatabaseConfig
	Oauth    OauthConfig
}

type ServerConfig struct {
	Address string // Address for webserver to listen on
	Port    string // Port for webserver to listen on
}

type DatabaseConfig struct {
	Driver   string                 // postgres or sqlite
	Postgres DatabasePostgresConfig // Postgres config
	Sqlite   DatabaseSqliteConfig   // sqlite config
}

type DatabasePostgresConfig struct {
	Address  string // Address of the postgres instance
	Port     string // Port of the postgres instance
	User     string // USer of the database
	Password string // Password of the database
	Name     string // Name of the database
}

type DatabaseSqliteConfig struct {
	Path string // Path to the sqlite database
}

type OauthConfig struct {
	Server   string
	Id       string
	Secret   string
	Callback string
}
