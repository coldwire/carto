package maps

import (
	"carto/models"
	errors "carto/utils/errs"
	"carto/utils/tokens"

	"github.com/gofiber/fiber/v2"
)

func Editor(c *fiber.Ctx) error {
	mapId := c.Params("id")

	request := struct {
		Username string `json:"username"`
	}{}

	ma := models.Map{
		Id: mapId,
	}

	err := c.BodyParser(&request)
	if err != nil {
		return errors.Handle(c, errors.ErrBody, err)
	}

	if request.Username == "" {
		return errors.Handle(c, errors.ErrBody, "please set a username")
	}

	token, err := tokens.Parse(c.Cookies("token")) // Parse user's JWT token
	if err != nil {
		return errors.Handle(c, errors.ErrAuth, err)
	}

	isEditor, err := ma.IsEditor(models.User{Username: token.Username})
	if err != nil {
		return errors.Handle(c, errors.ErrUnknown, err)
	}

	if !isEditor {
		return errors.Handle(c, errors.ErrPermission)
	}

	if c.Route().Method == "PUT" {
		err = ma.AddEditor(request.Username)
	} else if c.Route().Method == "DELETE" {
		err = ma.DelEditor(request.Username)
	} else {
		return errors.Handle(c, errors.ErrRequest)
	}

	if err != nil {
		return errors.Handle(c, errors.ErrUnknown, err)
	}

	return errors.Handle(c, errors.Success)
}
