package maps

import (
	"carto/models"
	errors "carto/utils/errs"
	"carto/utils/tokens"

	"github.com/alexedwards/argon2id"
	"github.com/gofiber/fiber/v2"
	"github.com/lithammer/shortuuid/v4"
)

func Create(c *fiber.Ctx) error {
	// Structure of the JSON request
	request := struct {
		Title       string `json:"title"`
		Description string `json:"description"`
		Password    string `json:"password"`

		Lat  float32 `json:"lat"`
		Long float32 `json:"long"`
		Zoom int     `json:"zoom"`
	}{}

	// Parse the body
	err := c.BodyParser(&request)
	if err != nil {
		return errors.Handle(c, errors.ErrBody, err)
	}

	// Check if mandatory fields are empty
	if request.Title == "" || request.Description == "" {
		return errors.Handle(c, errors.ErrBody, "Title or Description empty")
	}

	token, err := tokens.Parse(c.Cookies("token")) // Parse user's JWT token
	if err != nil {
		return errors.Handle(c, errors.ErrAuth, err)
	}

	// Prepare table to create
	var ma = models.Map{
		Id: shortuuid.New(),

		Title:       request.Title,
		Description: request.Description,

		Lat:  request.Lat,
		Long: request.Long,
		Zoom: request.Zoom,
	}

	// If password is set, hash it
	if request.Password != "" {
		h, err := argon2id.CreateHash(request.Password, argon2id.DefaultParams)
		if err != nil {
			return errors.Handle(c, errors.ErrUnknown, err)
		}

		ma.Password = h
	}

	// Insert table
	err = ma.Create()
	if err != nil {
		return errors.Handle(c, errors.ErrUnknown, err)
	}

	err = ma.AddEditor(token.Username)
	if err != nil {
		return errors.Handle(c, errors.ErrUnknown, err)
	}

	// Return success
	return errors.Handle(c, errors.Success, ma)
}
