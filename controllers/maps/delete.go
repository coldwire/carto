package maps

import (
	"carto/models"
	errors "carto/utils/errs"
	"carto/utils/tokens"

	"github.com/gofiber/fiber/v2"
)

func Delete(c *fiber.Ctx) error {
	mapId := c.Params("id")

	ma := models.Map{
		Id: mapId,
	}

	token, err := tokens.Parse(c.Cookies("token")) // Parse user's JWT token
	if err != nil {
		return errors.Handle(c, errors.ErrAuth, err)
	}

	isEditor, err := ma.IsEditor(models.User{Username: token.Username})
	if err != nil {
		return errors.Handle(c, errors.ErrUnknown, err)
	}

	if !isEditor {
		return errors.Handle(c, errors.ErrPermission)
	}

	err = ma.Delete()
	if err != nil {
		return errors.Handle(c, errors.ErrDatabaseRemove, err)
	}

	return errors.Handle(c, errors.Success)
}
