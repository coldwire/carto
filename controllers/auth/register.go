package auth

import (
	"carto/models"
	"carto/utils"
	errors "carto/utils/errs"
	"carto/utils/tokens"
	"regexp"
	"time"

	"github.com/alexedwards/argon2id"
	"github.com/gofiber/fiber/v2"
)

func Register(c *fiber.Ctx) error {
	request := struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}{}

	err := c.BodyParser(&request)
	if err != nil {
		return errors.Handle(c, errors.ErrBody, err)
	}

	var usr = models.User{
		Username: request.Username,
		AuthMode: "LOCAL",
	}

	usernameValidation, err := regexp.MatchString("[a-zA-Z]{3,}", request.Username)
	if !usernameValidation {
		return errors.Handle(c, errors.ErrBody, err)
	}

	if len(request.Password) <= 8 {
		return errors.Handle(c, errors.ErrBody, "password too short or invalid")
	}

	exist := usr.Exist()
	if exist {
		return errors.Handle(c, errors.ErrAuthExist)
	}

	hash, err := argon2id.CreateHash(request.Password, argon2id.DefaultParams)
	if err != nil {
		return errors.Handle(c, errors.ErrAuth, err)
	}

	usr.Password = hash
	err = usr.Create()
	if err != nil {
		return errors.Handle(c, errors.ErrDatabaseCreate, err)
	}

	token := tokens.Generate(tokens.Token{
		Username: request.Username,
		Role:     "user",
	}, 12*time.Hour)

	utils.SetCookie(c, "token", token, time.Now().Add(time.Hour*12))

	return errors.Handle(c, errors.Success, usr)
}
