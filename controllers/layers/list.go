package layers

import (
	"carto/models"
	errors "carto/utils/errs"
	"carto/utils/tokens"

	"github.com/gofiber/fiber/v2"
)

func List(c *fiber.Ctx) error {
	token, err := tokens.Parse(c.Cookies("token")) // Parse user's JWT token
	if err != nil {
		return errors.Handle(c, errors.ErrAuth, err)
	}

	maps, err := models.Layer{}.List(token.Username)

	return errors.Handle(c, errors.Success, maps)
}
