package layers

import (
	"carto/models"
	errors "carto/utils/errs"
	"carto/utils/tokens"

	"github.com/gofiber/fiber/v2"
)

func Editor(c *fiber.Ctx) error {
	layerId := c.Params("id")

	request := struct {
		Username string `json:"username"`
	}{}

	la := models.Map{
		Id: layerId,
	}

	err := c.BodyParser(&request)
	if err != nil {
		return errors.Handle(c, errors.ErrBody, err)
	}

	if request.Username == "" {
		return errors.Handle(c, errors.ErrBody, "please set a username")
	}

	token, err := tokens.Parse(c.Cookies("token")) // Parse user's JWT token
	if err != nil {
		return errors.Handle(c, errors.ErrAuth, err)
	}

	isEditor, err := la.IsEditor(models.User{Username: token.Username})
	if err != nil {
		return errors.Handle(c, errors.ErrUnknown, err)
	}

	if !isEditor {
		return errors.Handle(c, errors.ErrPermission)
	}

	if c.Route().Method == "PUT" {
		err = la.AddEditor(request.Username)
	} else if c.Route().Method == "DELETE" {
		err = la.DelEditor(request.Username)
	} else {
		return errors.Handle(c, errors.ErrRequest)
	}

	if err != nil {
		return errors.Handle(c, errors.ErrUnknown, err)
	}

	return errors.Handle(c, errors.Success)
}
