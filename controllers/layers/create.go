package layers

import (
	"carto/models"
	errors "carto/utils/errs"
	"carto/utils/tokens"

	"github.com/gofiber/fiber/v2"
	"github.com/lithammer/shortuuid/v4"
)

func Create(c *fiber.Ctx) error {
	// Structure of the JSON request
	request := struct {
		Title       string `json:"title"`
		Type        string `json:"type"`
		Description string `json:"description"`
		Icon        string `json:"icon"`
	}{}

	// Parse the body
	err := c.BodyParser(&request)
	if err != nil {
		return errors.Handle(c, errors.ErrBody, err)
	}

	// Check if mandatory fields are empty
	if request.Title == "" || request.Description == "" {
		return errors.Handle(c, errors.ErrBody, "Title or Description empty")
	}

	token, err := tokens.Parse(c.Cookies("token")) // Parse user's JWT token
	if err != nil {
		return errors.Handle(c, errors.ErrAuth, err)
	}

	// Prepare table to create
	var la = models.Layer{
		Id:          shortuuid.New(),
		Type:        request.Type,
		Title:       request.Title,
		Description: request.Description,
		Icon:        request.Icon,
	}

	// Insert table
	err = la.Create()
	if err != nil {
		return errors.Handle(c, errors.ErrUnknown, err)
	}

	err = la.AddEditor(token.Username)
	if err != nil {
		return errors.Handle(c, errors.ErrUnknown, err)
	}

	// Return success
	return errors.Handle(c, errors.Success, la)
}
