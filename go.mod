module carto

go 1.16

require (
	codeberg.org/coldwire/cwauth v0.0.0-20220419124813-3a1ccde34864
	github.com/BurntSushi/toml v1.1.0
	github.com/alexedwards/argon2id v0.0.0-20211130144151-3585854a6387 // indirect
	github.com/gofiber/fiber/v2 v2.32.0
	github.com/joho/godotenv v1.4.0
	github.com/kataras/jwt v0.1.8 // indirect
	github.com/lithammer/shortuuid/v4 v4.0.0 // indirect
	github.com/rs/zerolog v1.26.1
	golang.org/x/oauth2 v0.0.0-20220411215720-9780585627b5
	gorm.io/driver/postgres v1.3.4
	gorm.io/driver/sqlite v1.3.1
	gorm.io/gorm v1.23.4
)
