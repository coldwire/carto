package models

import "carto/database"

type User struct {
	Username string `gorm:"primaryKey;not null;unique;" json:"username"`
	Password string `json:"-"`
	Role     string `gorm:"not null;" json:"role"`
	AuthMode string `gorm:"not null;" json:"-"`
}

func (u User) Create() error {
	return database.DB.Create(&u).Error
}

func (u User) Delete() error {
	return database.DB.Delete(&u).Error
}

func (u User) Find() (User, error) {
	var usr User
	err := database.DB.Find(&usr, u).Error
	if err != nil {
		return User{}, err
	}

	return usr, nil
}

func (u User) Exist() bool {
	var usr User
	err := database.DB.Find(&usr, u).Error
	if err != nil {
		return false
	}

	if usr.Username != "" {
		return true
	}

	return false
}

func (u User) IsFirstOne() bool {
	var usr User
	err := database.DB.Limit(1).Find(&usr).Error
	if err != nil {
		return false
	}

	if usr.Username == "" {
		return true
	}

	return false
}

func (u User) SetPassword(new string) error {
	err := database.DB.Model(User{}).Where("name = ?", u.Username).Update("password", new).Error
	if err != nil {
		return err
	}

	return nil
}
