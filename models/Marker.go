package models

type Marker struct {
	Id string `gorm:"primaryKey;not null;unique;" json:"id"` // Unique ID of the layer

	Title       string  `gorm:"not null;" json:"title"`       // Title of the layer
	Description string  `gorm:"not null;" json:"description"` // Description of the layer
	Links       string  `gorm:"not null;" json:"links"`       // links
	Long        float32 `gorm:"not null;" json:"long"`        // Longitude of the marker
	Lat         float32 `gorm:"not null;" json:"lat"`         // Latitude of the marker
}
