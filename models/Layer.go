package models

import (
	"carto/database"

	"gorm.io/gorm/clause"
)

// A layer is a group of marker
type Layer struct {
	Id          string `gorm:"primaryKey;not null;unique;index" json:"id"`     // Unique ID of the layer
	Type        string `gorm:"not null;"                   json:"type"`        // Type of the layer
	Title       string `gorm:"not null;"                   json:"title"`       // Title of the layer
	Description string `gorm:"not null;"                   json:"description"` // Description of the layer
	Icon        string `gorm:"not null;"                   json:"icon"`        // Base64 encoded icon for markers

	Editors []User   `gorm:"many2many:layers_editors;" json:"editors"` // Users allowed to update this layer
	Markers []Marker `gorm:"foreignKey:Id"`
}

func (l Layer) Create() error {
	return database.DB.Create(&l).Error
}

func (l Layer) Delete() error {
	return database.DB.Select(clause.Associations).Delete(&l).Error
}

func (l Layer) Find() (Map, error) {
	var la Map
	err := database.DB.Find(&la, l).Error
	if err != nil {
		return Map{}, err
	}

	return la, nil
}

func (l Layer) List(username string) ([]Map, error) {
	var layerIds []string
	var la []Map

	err := database.DB.Raw("SELECT layer_id FROM layer_editors WHERE user_username = ?", username).Scan(&layerIds).Error

	if err != nil {
		return la, err
	}

	err = database.DB.Model(&la).Preload("Editors").Where("id IN ?", layerIds).Find(&la).Error
	if err != nil {
		return la, err
	}

	return la, err
}

func (l Layer) AddEditor(username string) error {
	return database.DB.Model(&l).Association("Editors").Append(&User{
		Username: username,
	})
}

func (l Layer) DelEditor(username string) error {
	return database.DB.Model(&l).Association("Editors").Delete(&User{
		Username: username,
	})
}

func (l Layer) IsEditor(u User) (bool, error) {
	var usr User
	err := database.DB.Model(&l).Where("username = ?", u.Username).Association("Editors").Find(&usr)
	if err != nil {
		return false, err
	}

	if usr.Username == "" {
		return false, err
	}

	return true, err
}
