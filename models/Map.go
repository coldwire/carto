package models

import (
	"carto/database"

	"gorm.io/gorm/clause"
)

type Map struct {
	Id string `gorm:"primaryKey;not null;unique;" json:"id"` // Unique ID of the map

	Title       string `gorm:"not null;" json:"title"`       // Title of the map
	Description string `gorm:"not null;" json:"description"` // Description of the map
	Password    string `json:"-"`                            // Password of the map (if empty it will be ignored)

	Lat  float32 `gorm:"not null;" json:"lat"`  // Initial latitude for the view
	Long float32 `gorm:"not null;" json:"long"` // Initial longitude for the view
	Zoom int     `gorm:"not null;" json:"zoom"` // Initial zoom for the view

	Editors []User  `gorm:"many2many:map_editors;" json:"editors"` // Users allowed to update this map
	Layers  []Layer `gorm:"many2many:map_layers;"  json:"layers"`  // Maps having access to this layer
}

func (m Map) Create() error {
	return database.DB.Create(&m).Error
}

func (m Map) Delete() error {
	return database.DB.Select(clause.Associations).Delete(&m).Error
}

func (m Map) Find() (Map, error) {
	var ma Map
	err := database.DB.Find(&ma, m).Error
	if err != nil {
		return Map{}, err
	}

	return ma, nil
}

func (m Map) List(username string) ([]Map, error) {
	var mapIds []string
	var ma []Map

	err := database.DB.Raw("SELECT map_id FROM map_editors WHERE user_username = ?", username).Scan(&mapIds).Error

	if err != nil {
		return ma, err
	}

	err = database.DB.Model(&ma).Preload("Editors").Where("id IN ?", mapIds).Find(&ma).Error
	if err != nil {
		return ma, err
	}

	return ma, err
}

func (m Map) AddEditor(username string) error {
	return database.DB.Model(&m).Association("Editors").Append(&User{
		Username: username,
	})
}

func (m Map) DelEditor(username string) error {
	return database.DB.Model(&m).Association("Editors").Delete(&User{
		Username: username,
	})
}

func (m Map) AddLayer(id string) error {
	return database.DB.Model(&m).Association("Layers").Append(&Layer{
		Id: id,
	})
}

func (m Map) DelLayer(id string) error {
	return database.DB.Model(&m).Association("Layers").Delete(&Layer{
		Id: id,
	})
}

func (m Map) IsEditor(u User) (bool, error) {
	var usr User
	err := database.DB.Model(&m).Where("username = ?", u.Username).Association("Editors").Find(&usr)
	if err != nil {
		return false, err
	}

	if usr.Username == "" {
		return false, err
	}

	return true, err
}
